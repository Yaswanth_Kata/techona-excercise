import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Game } from '../types/game';

@Injectable()
export class GamesService {

    constructor(private http: HttpClient) {
    }

    public getAllGames(): Promise<Game[]> {
        return this.http
            .get<Array<Game>>('./assets/games.json').toPromise();
    }
}
