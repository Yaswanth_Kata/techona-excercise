import { Component, Input } from '@angular/core';
import { Game } from 'src/app/types';

@Component({
  selector: 'techona-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent {
  @Input()
  public game: Game;

  constructor() {}
}
