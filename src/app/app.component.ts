import { Component, OnInit, OnDestroy } from '@angular/core';
import { GamesService } from './services';
import { Game } from './types';
import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'techona-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject<void>();

  public games: Game[];

  public filteredGames: Game[];

  public searchInputControl: FormControl;

  constructor(private gamesService: GamesService) {
    this.searchInputControl = new FormControl();

    // subscribe to the input Search control Key changes
    this.searchInputControl.valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(searchInput => {
        // if any input typed filter the array , return a shallow copy of whole array if the search is empty
        this.filteredGames = searchInput
          ? this.filterGames(searchInput)
          : this.games.slice();
      });
  }

  async ngOnInit() {
    this.games = await this.gamesService.getAllGames();
    if (this.games && this.games.length) {
      // load images from assets based on the title.
      this.loadImageUrls();
      this.filteredGames = this.games;
    }
  }

  private loadImageUrls(): void {
    for (let i = 0; i < this.games.length; i++) {
      this.games[i].imageUrl = this.getImageUrlBasedOnTitle(this.games[i]);
    }
  }

  private filterGames(name: string) {
    return this.games.filter(
      game => game.title.toLowerCase().indexOf(name.toLowerCase()) !== -1
    );
  }

  private getImageUrlBasedOnTitle(game: Game): string {
    if (game.title) {
      const imageUrl = game.title.replace(' ', '').toLowerCase() + '.png';
      return './assets/images/' + imageUrl;
    }
  }

  // unsubscribe from all subscriptions on component destroy
  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
